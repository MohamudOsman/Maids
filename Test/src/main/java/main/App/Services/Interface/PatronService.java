package main.App.Services.Interface;

import main.Entity.Book;
import main.Entity.Patron;

import java.util.List;

public interface PatronService {

    List<Patron> getList();

    Patron getID(Long id);

    Patron create(Patron patron);
    Patron update(Long id,Patron patron);

    void delete(Long id);

    void deleteAll();

}
