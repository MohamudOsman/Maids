package main.App.Services.Interface;

import main.Entity.Book;

import java.util.List;

public interface BookService {

    List<Book> getList();

    Book getID(Long id);

    Book create(Book book);
    Book update(Long id,Book book);

    public Book borrowAbook(Book book,boolean isBorrowed);

    void delete(Long id);

    void deleteAll();

}
