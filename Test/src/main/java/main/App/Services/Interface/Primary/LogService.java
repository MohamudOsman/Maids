package main.App.Services.Interface.Primary;

import main.Entity.Book;
import main.Entity.Log;

import java.util.List;

public interface LogService {

    List<Log> getList();

    Log getID(Long id);

    Log addLog(Log log);


}
