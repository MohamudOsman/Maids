package main.App.Services.Interface;

import main.Entity.Book;
import main.Entity.BorrowingRecord;

import java.util.List;

public interface BorrowingRecordService {

    BorrowingRecord create(Long bookId,Long patronId,BorrowingRecord borrowingRecord);
    BorrowingRecord update(Long bookId,Long patronId,BorrowingRecord newborrowingRecord);

}
