package main.App.Services.Interface.Primary;

import main.Entity.AppUser;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;


public interface UserService extends UserDetailsService {

    public AppUser save(AppUser user);

    public List<AppUser> findAll();

    public AppUser findById(Long id);

}
