package main.App.Services.Impl;

import main.App.Services.Interface.BookService;
import main.Entity.Book;
import main.ErrorHandling.TypeException.ConflictException;
import main.ErrorHandling.TypeException.NotFoundException;
import main.App.Repository.BookRep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;


@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRep bookRep;


    @Cacheable(value = "ListBook")
    @Override
    public List<Book> getList() {
        return bookRep.findAll();
    }


    @Cacheable(value = "book",key = "#bookId")
    @Override
    public Book getID(Long bookId) {
        try {
            return bookRep.findById(bookId).get();

        }catch (NoSuchElementException ex){
            throw new NotFoundException("Book Not Found");
        }
    }

    @CacheEvict(cacheNames = "ListBook",allEntries = true)
    @Override
    public Book create(Book book) {

        if(bookRep.findByIsbn(book.getIsbn())!=null){
            throw new ConflictException("Conflict Error data ISBN");
        }

        return bookRep.save(book);
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "ListBook", key = "#id",allEntries = true),
                    @CacheEvict(value = "book", key = "#id",allEntries = true)
            }
    )
    @Override
    public Book update(Long id,Book newbook) {
        Book oldBook;

        try {
            oldBook=bookRep.findById(id).get();

        }catch (NoSuchElementException ex){
            throw new NotFoundException("Book Not Found");
        }

        oldBook.setIsbn(newbook.getIsbn());
        oldBook.setTitle(newbook.getTitle());
        oldBook.setAuthor(newbook.getAuthor());

        return bookRep.save(oldBook);
    }

    public Book borrowAbook(Book book,boolean isBorrowed) {
        book.setBorrowed(isBorrowed);
        return bookRep.save(book);
    }

    @Caching(
            evict = {
                    @CacheEvict(cacheNames = "ListBook", key="#id",  allEntries = true),
                    @CacheEvict(cacheNames = "book", key="#id")
            }
    )
    @Override
    public void delete(Long id) {

        if(bookRep.findById(id).isEmpty()){
            throw new NotFoundException("Book Not Found");
        }
        bookRep.deleteById(id);
    }

    @Caching(
            evict = {
                    @CacheEvict(cacheNames = "ListBook", key="#id",  allEntries = true),
                    @CacheEvict(cacheNames = "book", key="#id",  allEntries = true)
            }
    )
    @Override
    public void deleteAll() {
        bookRep.deleteAll();
    }


}
