package main.App.Services.Impl;

import main.App.Services.Interface.PatronService;
import main.Entity.Patron;
import main.ErrorHandling.TypeException.NotFoundException;
import main.App.Repository.PatronRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PatronServiceImpl implements PatronService {
    
    @Autowired
    private PatronRep patronRep;

    @Override
    public List<Patron> getList() {
        return patronRep.findAll();
    }

    @Override
    public Patron getID(Long id) {
        try {
            return patronRep.findById(id).get();

        }catch (NoSuchElementException ex){
            throw new NotFoundException("Patron Not Found");
        }
    }

    @Override
    public Patron create(Patron patron) {

        return patronRep.save(patron);

    }

    @Override
    public Patron update(Long id,Patron newpatron) {
        Patron oldPatron;

        try {
            oldPatron=patronRep.findById(id).get();

        }catch (NoSuchElementException ex){
            throw new NotFoundException("Patron Not Found");
        }

        oldPatron.setName(newpatron.getName());
        oldPatron.setContactInformation(newpatron.getContactInformation());

        return patronRep.save(oldPatron);
    }

    @Override
    public void delete(Long id) {

        if(patronRep.findById(id).isEmpty()){
            throw new NotFoundException("Patron Not Found");
        }
        patronRep.deleteById(id);
    }

    @Override
    public void deleteAll() {
        patronRep.deleteAll();
    }


}
