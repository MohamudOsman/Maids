package main.App.Services.Impl;

import main.App.Services.Interface.BookService;
import main.App.Services.Interface.BorrowingRecordService;
import main.App.Services.Interface.PatronService;
import main.Entity.Book;
import main.Entity.BorrowingRecord;
import main.ErrorHandling.TypeException.BadRequestException;
import main.ErrorHandling.TypeException.LockedException;
import main.ErrorHandling.TypeException.NotFoundException;
import main.App.Repository.BorrowingRecordRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
public class BorrowingRecordServiceImpl implements BorrowingRecordService {
    
    @Autowired
    BorrowingRecordRep borrowingRecordRep;

    @Autowired
    BookService bookService;

    @Autowired
    PatronService patronService;

    @Transactional
    @Override
    public BorrowingRecord create(Long bookId,Long patronId,BorrowingRecord borrowingRecord) {

        if(borrowingRecord.getBorrowingDate()==null){
            throw new BadRequestException("Borrowing Date Not Found");
        }
        Book book=bookService.getID(bookId);
        if(!checkIsBorrowed(book)){
            book=bookService.borrowAbook(book,true);

            borrowingRecord.setBook(book);
            borrowingRecord.setPatron(patronService.getID(patronId));

            return borrowingRecordRep.save(borrowingRecord);
        }else{
            throw new LockedException("This book is borrowed");
        }

    }

    @Transactional
    @Override
    public BorrowingRecord update(Long bookId,Long patronId,BorrowingRecord newborrowingRecord) {

        if(newborrowingRecord.getReturnDate()==null){
            throw new BadRequestException("Return Date Not Found");
        }


        BorrowingRecord oldBorrowingRecord=getBorrowingRecordByBookIdAndPatronId(bookId,patronId);

        if(oldBorrowingRecord==null){
            throw new NotFoundException("No book borrowing found.check the bookId or patronId");
        }

        if(checkIsBorrowed(oldBorrowingRecord.getBook())){

            bookService.borrowAbook(oldBorrowingRecord.getBook(),false);

            if(newborrowingRecord.getBorrowingDate()!=null)
                oldBorrowingRecord.setBorrowingDate(newborrowingRecord.getBorrowingDate());
            oldBorrowingRecord.setReturnDate(newborrowingRecord.getReturnDate());

            return borrowingRecordRep.save(oldBorrowingRecord);
        }else{
            throw new BadRequestException("This book is not borrowed");
        }

    }


    public boolean checkIsBorrowed(Book book) {
        return book.isBorrowed();
    }

    public BorrowingRecord getBorrowingRecordByBookIdAndPatronId(Long bookId,Long patronId) {
        try {
            return  borrowingRecordRep.getBorrowingByBookIdAndPatronId(bookId,patronId);

        }catch (NoSuchElementException ex){
            throw new NotFoundException("BorrowingRecord Not Found");
        }

    }

}
