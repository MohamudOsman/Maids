package main.App.Services.Impl.Primary;

import main.App.Services.Interface.Primary.LogService;
import main.Entity.Log;
import main.ErrorHandling.TypeException.NotFoundException;
import main.App.Repository.Primary.LogRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogRep logRep;

    @Override
    public List<Log> getList() {
        return logRep.findAll();
    }

    @Override
    public Log getID(Long id) {
        try {
            return logRep.findById(id).get();

        }catch (NoSuchElementException ex){
            throw new NotFoundException("Log Not Found");
        }
    }

    @Override
    public Log addLog(Log log) {

        return logRep.save(log);
    }


}
