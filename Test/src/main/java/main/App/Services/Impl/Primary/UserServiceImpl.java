package main.App.Services.Impl.Primary;

import main.App.Services.Interface.Primary.UserService;
import main.Entity.AppUser;
import main.ErrorHandling.TypeException.ConflictException;
import main.ErrorHandling.TypeException.NotFoundException;
import main.Security.UserInformation;
import main.App.Repository.Primary.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Bean
    private PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //return new User("Mohamud",passwordEncoder().encode("Password"), AuthorityUtils.NO_AUTHORITIES);
        //AppUser user= userRepository.findByEmail(username);
        AppUser appUser= userRepository.findByUsername(username);
        UserInformation user=new UserInformation(appUser);
        if(user==null){
            throw new NotFoundException("user Not Found");
        }
        return user;
    }

    public AppUser save(AppUser user){
        AppUser temp=userRepository.findByUsername(user.getUsername());
        if(temp!=null){
            if(user.getEmail().equals(temp.getEmail())){
            throw new ConflictException("This User is Found");
            }
        }
        user.setPassword(passwordEncoder().encode(user.getPassword()));
        return userRepository.save(user);
    }

    public List<AppUser> findAll() {
        return userRepository.findAll();
    }

    public AppUser findById(Long id) {
        try {
            return userRepository.findById(id).get();

        }catch (NoSuchElementException ex){
            throw new NotFoundException("User Not Found");
        }
    }

}
