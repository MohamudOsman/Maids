package main.App.Repository.Primary;

import main.Entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LogRep extends JpaRepository<Log,Long> {


}
