package main.App.Repository;


import main.Entity.BorrowingRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BorrowingRecordRep extends JpaRepository<BorrowingRecord,Long> {

    @Query("SELECT b FROM BorrowingRecord b WHERE b.patron.id=:patronId AND b.book.id=:bookId AND b.book.isBorrowed=true ORDER BY b.created_at DESC limit 1")
    BorrowingRecord getBorrowingByBookIdAndPatronId(Long bookId, Long patronId);

}
