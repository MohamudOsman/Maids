package main.App.Repository;


import main.Entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRep extends JpaRepository<Book,Long> {

    Book findByIsbn(String isbn);
}
