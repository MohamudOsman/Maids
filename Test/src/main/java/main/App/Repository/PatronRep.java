package main.App.Repository;

import main.Entity.Patron;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatronRep extends JpaRepository<Patron,Long> {
}
