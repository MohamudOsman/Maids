package main.App.Controller.Primary;


import main.Data.Security.ResponseJWTData;
import main.Data.Security.ResponseRegister;
import main.Data.Security.RequestLogin;
import main.Entity.AppUser;
import main.Security.*;
import main.App.Services.Impl.Primary.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/auth")

public class AuthController {

    @Autowired
    TokenUtiles tokenUtiles;

    @Autowired
    UserServiceImpl userService;

    @Autowired
    AuthenticationManager authenticationManager;


    @GetMapping(value = {"/signin"})
    public ResponseEntity<ResponseJWTData> signIn (@RequestBody RequestLogin requestLogin){

        final Authentication authentication=authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(requestLogin.getUsername(), requestLogin.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);


        UserDetails userDetails=userService.loadUserByUsername(requestLogin.getUsername());
        ResponseJWTData responseJWTData =new ResponseJWTData(tokenUtiles.tokenGenerate(userDetails));

//        return responseJWTData;
        return new ResponseEntity<>(responseJWTData,HttpStatus.OK);
    }

    @PostMapping(value = {"/register"})
    public ResponseEntity<ResponseRegister> Register (@RequestBody AppUser user){

        userService.save(user);
        return new ResponseEntity<>(new ResponseRegister(user.getUsername(),user.getEmail()), HttpStatus.CREATED);
    }


}
