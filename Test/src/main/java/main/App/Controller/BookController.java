package main.App.Controller;


import main.App.Services.Interface.BookService;
import main.Entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/api/books")
public class BookController {

    @Autowired
    BookService bookService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value={"/",""})
    public ResponseEntity<List<Book>> listBook(){
        return new ResponseEntity<>(bookService.getList(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value={"/{id}"})
    public ResponseEntity<Book> getID(@PathVariable(name = "id",required = true) Long bookId){

        return new ResponseEntity<>(bookService.getID(bookId),HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value={"","/"})
    public ResponseEntity<Book> create(@Valid @RequestBody Book book){

        return new ResponseEntity<>(bookService.create(book),HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value={"/{id}"})
    public ResponseEntity<Book> update(@PathVariable(name = "id",required = true) Long bookId,@Valid @RequestBody Book book){

        return new ResponseEntity<>(bookService.update(bookId,book),HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value={"/{id}"})
    public ResponseEntity<Void> deleteBook(@PathVariable(name = "id",required = true) Long bookId){

        bookService.delete(bookId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value={"/deleteAll"})
    public ResponseEntity<Void> deleteAll(){

        bookService.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
