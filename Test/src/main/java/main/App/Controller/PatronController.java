package main.App.Controller;


import main.App.Services.Interface.PatronService;
import main.Entity.Patron;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/api/patrons")
public class PatronController {

    @Autowired
    PatronService patronService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value={"/",""})
    public ResponseEntity<List<Patron>> listPatron(){
        return new ResponseEntity<>(patronService.getList(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value={"/{id}"})
    public ResponseEntity<Patron> getID(@PathVariable(name = "id",required = true) Long bookId){

        return new ResponseEntity<>(patronService.getID(bookId),HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value={"","/"})
    public ResponseEntity<Patron> create(@Valid @RequestBody Patron book){

        return new ResponseEntity<>(patronService.create(book),HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value={"/{id}"})
    public ResponseEntity<Patron> update(@PathVariable(name = "id",required = true) Long bookId,@Valid @RequestBody Patron book){

        return new ResponseEntity<>(patronService.update(bookId,book),HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value={"/{id}"})
    public ResponseEntity<Void> deletePatron(@PathVariable(name = "id",required = true) Long bookId){

        patronService.delete(bookId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value={"/deleteAll"})
    public ResponseEntity<Void> deleteAll(){

        patronService.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
