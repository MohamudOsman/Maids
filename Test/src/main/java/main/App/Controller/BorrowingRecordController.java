package main.App.Controller;



import main.App.Services.Interface.BorrowingRecordService;
import main.Entity.BorrowingRecord;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;


@RestController
@RequestMapping(value="/api")
public class BorrowingRecordController {

    @Autowired
    BorrowingRecordService borrowingRecordService;


    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value={"/borrow/{bookId}/patron/{patronId}"})
    public ResponseEntity<BorrowingRecord> create(@PathVariable(name = "bookId",required = true) Long bookId,
                                                  @PathVariable(name = "patronId",required = true) Long patronId,
                                                  @Valid @RequestBody BorrowingRecord borrowingRecord)
    {


        return new ResponseEntity<>(borrowingRecordService.create(bookId,patronId,borrowingRecord),HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value={"/return/{bookId}/patron/{patronId}"})
    public ResponseEntity<BorrowingRecord> update(@PathVariable(name = "bookId",required = true) Long bookId,
                                                  @PathVariable(name = "patronId",required = true) Long patronId,
                                                  @Valid @RequestBody BorrowingRecord borrowingRecord)
    {

        return new ResponseEntity<>(borrowingRecordService.update(bookId,patronId,borrowingRecord),HttpStatus.OK);
    }


}
