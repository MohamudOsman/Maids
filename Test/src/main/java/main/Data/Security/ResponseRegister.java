package main.Data.Security;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@Getter
@Setter
public class ResponseRegister {

    private String username;
    private String email;
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    private Date created;

    public ResponseRegister(String username, String email) {
        this.username = username;
        this.email = email;
        this.created = new Date();
    }
}
