package main.Data.Security;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseJWTData {

    private String token;

}
