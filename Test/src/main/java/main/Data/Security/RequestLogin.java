package main.Data.Security;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class RequestLogin {

    private String username;
    private String password;
    private String email;

}
