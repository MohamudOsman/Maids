package main.Aspect;


import main.Entity.Log;
import main.ErrorHandling.TypeException.AuthenticationException;
import main.Security.UserInformation;
import main.App.Services.Interface.Primary.LogService;
import main.App.Services.Interface.Primary.UserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;



@Aspect
@Order(1)
@Component
public class Logging{

    @Autowired
    LogService logService;

    @Autowired
    UserService userService;


    @Pointcut(value = "execution(* main.Repository.*.*(..))")
    public void forRepositoryLog() {}

    @Pointcut(value = "execution(* main.Services.*.*(..))")
    public void forServiceLog () {}

    @Pointcut(value = "execution(* main.Controller.*.*(..))")
    public void forControllerLog () {}

    @Pointcut(value = "forRepositoryLog() || forServiceLog() || forControllerLog()")
    public void forAllApp() {}


	@After(value = "forControllerLog()")
    public void ResponseLog(JoinPoint joinPoint){

        String methodName = joinPoint.getSignature().toShortString().replace("(..)","");

        String parameter="";
        Object [] args = joinPoint.getArgs();
        for (Object arg : args) {
            if(arg.equals(args[args.length-1])){
                parameter+=arg.toString();
            }else {
                parameter+=arg.toString()+",";
            }
        }

        try{
            UserInformation user=(UserInformation) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if(user!=null){
                logService.addLog(new Log(0L,methodName,parameter,null,userService.findById(user.getId())));
            }
        }catch (RuntimeException ex){
            throw new AuthenticationException("Error authenticated");
        }
    }

}
