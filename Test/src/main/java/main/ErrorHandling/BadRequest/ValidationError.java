package main.ErrorHandling.BadRequest;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class ValidationError {

    List<String> errors;
    String uri;
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    Timestamp timestamp;

    public ValidationError() {
        this.errors=new ArrayList<>();
        this.timestamp=new Timestamp(System.currentTimeMillis());
    }

    public ValidationError(String uri) {
        this();
        this.uri = uri;
    }

    public void addError(String error){
        this.errors.add(error);
    }

}
