package main.ErrorHandling.TypeException;

import org.springframework.http.HttpStatus;

public class BadRequestException extends APIBaseException {

    public BadRequestException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatusCode(){
        return HttpStatus.BAD_REQUEST;
    }


}
