package main.ErrorHandling.TypeException;

import org.springframework.http.HttpStatus;

public class LockedException extends APIBaseException {

    public LockedException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatusCode() {
        return HttpStatus.LOCKED;
    }
}
