package main.ErrorHandling.TypeException;

import org.springframework.http.HttpStatus;


public class AuthenticationException extends APIBaseException {

    public AuthenticationException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatusCode(){
        return HttpStatus.FORBIDDEN;
    }


}