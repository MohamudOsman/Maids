package main.ErrorHandling.TypeException;

import org.springframework.http.HttpStatus;

public class ConflictException extends APIBaseException {

    public ConflictException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatusCode() {
        return HttpStatus.CONFLICT;
    }
}
