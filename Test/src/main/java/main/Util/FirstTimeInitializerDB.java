package main.Util;



import main.Data.RoleStatus;
import main.Entity.AppUser;
import main.App.Services.Impl.Primary.UserServiceImpl;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class FirstTimeInitializerDB implements CommandLineRunner {

    private final Log logger = LogFactory.getLog(FirstTimeInitializerDB.class);

    @Autowired
    private UserServiceImpl userService;

    @Override
    public void run(String... strings) throws Exception {

        if (userService.findAll().isEmpty()) {
            logger.info("No Users accounts found. Creating some users");

            AppUser user = new AppUser(0,"test", "password", "test@gmail.com", RoleStatus.ADMIN,null,null);
            userService.save(user);
        }
    }



}
