package main.Entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;


@Entity(name="Book")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "ISBN is Required")
    @Size(min = 10,max = 13,message = "ISBN letter between 10-13")
    @Column(name="ISBN",unique = true)
    private String isbn;

    @NotNull(message = "Title is Required")
    @Size(min = 5,max = 50,message = "Title letter between 5-50")
    private String title;

    @NotNull(message = "Author is Required")
    @Size(min = 5,max = 50,message = "Author letter between 5-50")
    private String author;

    @Column()
    private boolean isBorrowed=false;

    @JsonIgnore
    private boolean isDeleted;

    @CreationTimestamp
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    @Column(name = "created_at")
    private Timestamp created_at;

    @UpdateTimestamp
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    @Column(name ="updated_at")
    private Timestamp updated_at;


    //Relation with BorrowingRecord
    @OneToMany(mappedBy = "book",cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<BorrowingRecord> borrowedBooks=new HashSet<>();

}
