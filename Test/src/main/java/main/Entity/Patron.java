package main.Entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity()
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Patron {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Name is Required")
    @Size(min = 5,max = 50,message = "name letter between 5-50")
    private String name;


    @NotNull(message = "contactInformation is Required")
    @Size(min = 5,max = 50,message = "contactInformation letter between 5-50")
    private String contactInformation;

    @JsonIgnore
    private boolean isDeleted;

    @CreationTimestamp
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    @Column(name = "created_at")
    private Timestamp created_at;

    @UpdateTimestamp
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    @Column(name ="updated_at")
    private Timestamp updated_at;


    //Relation with BorrowingRecord
    @OneToMany(mappedBy = "patron",cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<BorrowingRecord> borrowedBooks=new HashSet<>();

}
