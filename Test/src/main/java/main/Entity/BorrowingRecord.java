package main.Entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Entity()
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BorrowingRecord  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    private Timestamp borrowingDate;

    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    private Timestamp returnDate;

    @JsonIgnore
    private boolean isDeleted;

    @CreationTimestamp
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    @Column(name = "created_at")
    private Timestamp created_at;

    @UpdateTimestamp
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss")
    @Column(name ="updated_at")
    private Timestamp updated_at;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book",nullable = false)
    @JsonIgnoreProperties({"isbn","created_at","updated_at"})
//    @JsonIgnoreProperties({"id","author","title"})
    private Book book;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patron",nullable = false)
    @JsonIgnoreProperties({"created_at","updated_at"})
//    @JsonIgnoreProperties({"id","contactInformation","name"})
    private Patron patron;


}
