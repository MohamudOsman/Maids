package main;

import main.App.Controller.PatronController;
import main.Entity.Patron;
import main.App.Services.Interface.PatronService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest()
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PatronsControllerTest {

    @Autowired
    PatronController patronController;

    @MockBean
    PatronService patronService;


    @Test
    void listPatronTest(){
        List<Patron> patronsMock=new ArrayList<>();
        patronsMock.add(new Patron(1L,"Mohamud","099999999",false,null,null,null));
        patronsMock.add(new Patron(2L,"Ahmad","0888888888",false,null,null,null));

        Mockito.when(patronService.getList()).thenReturn(patronsMock);


        List<Patron> patrons=patronService.getList();
        assertEquals(2,patrons.size());
        assertEquals(1L,patrons.get(0).getId());
        assertEquals(2L,patrons.get(1).getId());

    }
    @Test
    void getPatronIDTest(){
        Patron patronMock=new Patron(1L,"Ali","0777777777",false,null,null,null);
        Mockito.when(patronService.getID(Mockito.anyLong())).thenReturn(patronMock);

        Patron patron=patronService.getID(1L);
        assertEquals(1L,patron.getId());
        assertEquals("Ali",patron.getName());
        assertEquals("0777777777",patron.getContactInformation());

    }


}
