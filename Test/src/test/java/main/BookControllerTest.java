package main;


import main.App.Controller.BookController;
import main.Entity.Book;
import main.App.Services.Interface.BookService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest()
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BookControllerTest {

    @Autowired
    BookController bookController;

    @MockBean
    BookService bookSrvice;


    @BeforeAll
    public void initMockito() {

    }

    @Test
    void listBookTest(){
        List<Book> booksMock=new ArrayList<>();
        booksMock.add(new Book(1L,"123ss123ss123","book1","Mohamud",false,false,null,null,null));
        booksMock.add(new Book(2L,"123xx123xx123","book2","Ahmad",true,false,null,null,null));
        booksMock.add(new Book(3L,"123zz123zz123","book3","Ali",false,false,null,null,null));

        Mockito.when(bookSrvice.getList()).thenReturn(booksMock);


        List<Book> books=bookSrvice.getList();
        assertEquals(3,books.size());
        assertEquals(1L,books.get(0).getId());
        assertEquals(2L,books.get(1).getId());
        assertEquals(3L,books.get(2).getId());

    }
    @Test
    void getIDTest(){
        Book bookMock=new Book(1L,"123ss123ss123","book1","Mohamud",false,false,null,null,null);
        Mockito.when(bookSrvice.getID(Mockito.anyLong())).thenReturn(bookMock);

        Book book=bookSrvice.getID(1L);
        assertEquals(1L,book.getId());
        assertEquals("123ss123ss123",book.getIsbn());
        assertEquals("book1",book.getTitle());
        assertEquals("Mohamud",book.getAuthor());
        assertEquals(false,book.isBorrowed());

    }

}
